# SingleSheet

Single sheet document/application to store things like character sheets on local devices. Functions completely client-side, depends on no external libraries, runs in browsers.